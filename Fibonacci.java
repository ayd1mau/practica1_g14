public class Fibonacci{


    public String serireFibonacci(int numero){
        //8.2 Bugfix
        if(numero >= 100){
            return "Error el numero tiene que ser menor a 100.";
        }
        int fibo1,fibo2,i;
        String cad="";
        fibo1=1;
        fibo2=1;

        for(i=2;i<=numero;i++){
            cad+=fibo2;
            fibo2 = fibo1 + fibo2;
            fibo1 = fibo2 - fibo1;
        }
        return cad;
    }
}