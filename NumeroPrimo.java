package practica1_g14

public class NumeroPrimo{

	public boolean EsPrimo(int num1, int num2, int num3){
		int numero = num1 + num2 + num3;
		int contador = 0;
		for(int i=0;i<=numero; i++){
			if((numero%i)==0)
				contador++;
		}

		if(contador <= 2)
			return true;

		return false;
	}
}
