public class Triangulo {
    public Double base;
    public Double altura;
    public Double area;

    public static void main(Double base, Double altura ){
        this.base = base;
        this.altura = altura;
    }

    public Double calcularArea() {
        this.area = (base*altura)/2;
    }

    public Double getArea() {
        return this.area;
    }
}