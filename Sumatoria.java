class Sumatoria {

    static void sucesiva(int desde, int hasta) {
        if (desde < hasta) {
            return;
        }

        for (int i = desde; i < hasta; i++) {
            DoLogger.log().info(i.toString());
        }
    }

}